# Simple Data Flow Example

Repository containing code pointed out in the RTSS 2020 submitted paper

## Structure
- `./simple-dflow-rev2.scade`: SCADE textual original code
- `./kcg/`: folder containing the generated C code, as well as auxiliary files:
  - `./kcg/alloc-file.json`: initial mapping and scheduling file
  - `./kcg/mapping.xml`: helper mapping file that contains information about the
    communication structure, dependencies, etc. and is mainly used to produce
    the orchestration code
