/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:28
*************************************************************$ */

#include "kcg_types.h"

#ifdef kcg_use_N0_task_out_ch_o2_type
kcg_bool kcg_comp_N0_task_out_ch_o2_type(
  N0_task_out_ch_o2_type *kcg_c1,
  N0_task_out_ch_o2_type *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->o2 == kcg_c2->o2);
  return kcg_equ;
}
#endif /* kcg_use_N0_task_out_ch_o2_type */

#ifdef kcg_use_struct_533
kcg_bool kcg_comp_struct_533(struct_533 *kcg_c1, struct_533 *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->i == kcg_c2->i);
  return kcg_equ;
}
#endif /* kcg_use_struct_533 */

#ifdef kcg_use_N0_task_out_ch_o1_type
kcg_bool kcg_comp_N0_task_out_ch_o1_type(
  N0_task_out_ch_o1_type *kcg_c1,
  N0_task_out_ch_o1_type *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->o1 == kcg_c2->o1);
  return kcg_equ;
}
#endif /* kcg_use_N0_task_out_ch_o1_type */

#ifdef kcg_use_struct_543
kcg_bool kcg_comp_struct_543(struct_543 *kcg_c1, struct_543 *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->o == kcg_c2->o);
  return kcg_equ;
}
#endif /* kcg_use_struct_543 */

/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** kcg_types.c
** Generation date: 2019-08-16T17:40:28
*************************************************************$ */

