/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "N4.h"

/* N4 */
kcg_int32 N4(
  /* i1 */
  kcg_int32 i1,
  /* i2 */
  kcg_int32 i2,
  /* i3 */
  kcg_int32 i3)
{
  /* o */
  kcg_int32 o;

  o = i1 + i2 + i3;
  return o;
}



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N4.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

