/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */
#ifndef _N4_H_
#define _N4_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* N4 */
extern kcg_int32 N4(
  /* i1 */
  kcg_int32 i1,
  /* i2 */
  kcg_int32 i2,
  /* i3 */
  kcg_int32 i3);



#endif /* _N4_H_ */
/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N4.h
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

