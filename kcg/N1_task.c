/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "N1_task.h"

/* c=(N1)/ */
void N1_task(outC_N1_task *outC)
{
  (*outC->N1_task_out_ch).o = /* c=(N1)/ */ N1((*outC->N0_task_out_ch_o1).o1);
}

#ifndef KCG_USER_DEFINED_INIT
void N1_task_init(outC_N1_task *outC)
{
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void N1_task_reset(outC_N1_task *outC)
{
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N1_task.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

