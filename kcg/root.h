/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */
#ifndef _root_H_
#define _root_H_

#include "kcg_types.h"

/* ========================  input structure  ====================== */
typedef struct { kcg_int32 /* i */ i; } inC_root;

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* ---------------------------  outputs  --------------------------- */
  kcg_int32 /* o */ o;
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* -------------------------  channels  ---------------------------- */
  N0_task_in_ch_type * /* a=(N0)/ */ N0_task_in_ch;
  N3_task_in_ch_type * /* e=(N3)/ */ N3_task_in_ch;
  N4_task_out_ch_type * /* o=(N4)/ */ N4_task_out_ch;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_root;

/* ===========  node initialization and cycle functions  =========== */
/* root */
extern void root_2(outC_root *outC);
/* root */
extern void root_1(inC_root *inC, outC_root *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void root_reset(outC_root *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void root_init(outC_root *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _root_H_ */
/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** root.h
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

