/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */
#ifndef _N3_task_H_
#define _N3_task_H_

#include "kcg_types.h"
#include "N3.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* --------------------- no memorised outputs  --------------------- */
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* -------------------------  channels  ---------------------------- */
  N3_task_out_ch_type * /* e=(N3)/ */ N3_task_out_ch;
  N3_task_in_ch_type * /* e=(N3)/ */ N3_task_in_ch;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_N3_task;

/* ===========  node initialization and cycle functions  =========== */
/* e=(N3)/ */
extern void N3_task(outC_N3_task *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void N3_task_reset(outC_N3_task *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void N3_task_init(outC_N3_task *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _N3_task_H_ */
/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N3_task.h
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

