/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "N0.h"

/* N0 */
void N0(/* i */ kcg_int32 i, /* o1 */ kcg_int32 *o1, /* o2 */ kcg_int32 *o2)
{
  *o1 = i + kcg_lit_int32(3);
  *o2 = i + kcg_lit_int32(5);
}



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N0.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

