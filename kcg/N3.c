/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "N3.h"

/* N3 */
kcg_int32 N3(/* i */ kcg_int32 i)
{
  /* o */
  kcg_int32 o;

  o = i + kcg_lit_int32(7);
  return o;
}



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N3.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

