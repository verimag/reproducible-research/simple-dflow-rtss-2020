/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "N3_task.h"

/* e=(N3)/ */
void N3_task(outC_N3_task *outC)
{
  (*outC->N3_task_out_ch).o = /* e=(N3)/ */ N3((*outC->N3_task_in_ch).i);
}

#ifndef KCG_USER_DEFINED_INIT
void N3_task_init(outC_N3_task *outC)
{
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void N3_task_reset(outC_N3_task *outC)
{
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N3_task.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

