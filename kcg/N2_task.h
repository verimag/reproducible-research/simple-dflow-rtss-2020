/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */
#ifndef _N2_task_H_
#define _N2_task_H_

#include "kcg_types.h"
#include "N2.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* --------------------- no memorised outputs  --------------------- */
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* -------------------------  channels  ---------------------------- */
  N2_task_out_ch_type * /* d=(N2)/ */ N2_task_out_ch;
  N0_task_out_ch_o2_type * /* a=(N0)/, o1, o2 */ N0_task_out_ch_o2;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_N2_task;

/* ===========  node initialization and cycle functions  =========== */
/* d=(N2)/ */
extern void N2_task(outC_N2_task *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void N2_task_reset(outC_N2_task *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void N2_task_init(outC_N2_task *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _N2_task_H_ */
/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N2_task.h
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

