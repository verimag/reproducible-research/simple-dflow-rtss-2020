/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "N2_task.h"

/* d=(N2)/ */
void N2_task(outC_N2_task *outC)
{
  (*outC->N2_task_out_ch).o = /* d=(N2)/ */ N2((*outC->N0_task_out_ch_o2).o2);
}

#ifndef KCG_USER_DEFINED_INIT
void N2_task_init(outC_N2_task *outC)
{
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void N2_task_reset(outC_N2_task *outC)
{
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N2_task.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

