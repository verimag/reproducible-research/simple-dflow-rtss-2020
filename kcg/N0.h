/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:28
*************************************************************$ */
#ifndef _N0_H_
#define _N0_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* N0 */
extern void N0(
  /* i */
  kcg_int32 i,
  /* o1 */
  kcg_int32 *o1,
  /* o2 */
  kcg_int32 *o2);



#endif /* _N0_H_ */
/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N0.h
** Generation date: 2019-08-16T17:40:28
*************************************************************$ */

