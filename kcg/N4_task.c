/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "N4_task.h"

/* o=(N4)/ */
void N4_task(outC_N4_task *outC)
{
  (*outC->N4_task_out_ch).o = /* o=(N4)/ */
    N4(
      (*outC->N1_task_out_ch).o,
      (*outC->N2_task_out_ch).o,
      (*outC->N3_task_out_ch).o);
}

#ifndef KCG_USER_DEFINED_INIT
void N4_task_init(outC_N4_task *outC)
{
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void N4_task_reset(outC_N4_task *outC)
{
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N4_task.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

