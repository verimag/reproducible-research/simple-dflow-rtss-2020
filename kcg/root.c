/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "root.h"

/* root */
void root_1(inC_root *inC, outC_root *outC)
{
  (*outC->N3_task_in_ch).i = inC->i;
  (*outC->N0_task_in_ch).i = inC->i;
}
/* root */
void root_2(outC_root *outC)
{
  outC->o = (*outC->N4_task_out_ch).o;
}

#ifndef KCG_USER_DEFINED_INIT
void root_init(outC_root *outC)
{
  outC->o = kcg_lit_int32(0);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void root_reset(outC_root *outC)
{
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** root.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

