/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** Command: mcg.exe -root root -target C simple-dflow-rev2.scade
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "N0_task.h"

/* a=(N0)/ */
void N0_task(outC_N0_task *outC)
{
  /* a=(N0)/ */
  N0(
    (*outC->N0_task_in_ch).i,
    &(*outC->N0_task_out_ch_o1).o1,
    &(*outC->N0_task_out_ch_o2).o2);
}

#ifndef KCG_USER_DEFINED_INIT
void N0_task_init(outC_N0_task *outC)
{
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void N0_task_reset(outC_N0_task *outC)
{
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $*** SCADE Suite KCG 64-bit 20.0 2019 R1 (build 20181212) ****
** N0_task.c
** Generation date: 2019-08-16T17:40:29
*************************************************************$ */

